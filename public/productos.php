<?php
session_start();
include("php/objectesProductes.php");
if(isset($_SESSION['cistella'])){
    $laMevaCistella = unserialize($_SESSION['cistella']);
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Juan Electronics</title>
        <link rel='stylesheet' href="css/style.css">
        <link rel='stylesheet' href="css/guia.css">
        <link rel='stylesheet' href="css/menus.css">
        <link rel='stylesheet' href="css/productos.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Maven+Pro:wght@400;700&family=Rubik:wght@400;700&display=swap" rel="stylesheet"> 
    </head>
    <body>
        <header>
            <img class="logo-navbar-mobile mobile" src="img/icons/icono-menu.png" onclick="menu()">
            <img class = "logo-juan" src="img/logo_juan.svg">
            <div class="top-navbar desktop">
                <div class="header-dropdown-menu">
                    <a href="index.php">Inicio</a>
                    <div class="header-dropdown-content">
                        <a href="index.php#productos">Productos destacados</a>
                        <a href="index.php#noticias">Noticias</a>
                        <a href="index.php#contacto">Contacto</a>
                    </div>
                </div>                
                <a href="">Catálogo</a>

                <input class="btn-carro" type="image" onclick="openCesta()" src="img/icons/cesta-icon.png">
            </div>
            <input class="btn-carro-mobile mobile" type="image" onclick="openCesta()" src="img/icons/cesta-icon.png"/>
        </header>

        <main>
            <div id="top" class="top-navbar-mobile mobile">
                <div class="menu-index-point">
                    <a href="index.php">Inicio</a>
                    <input type="image" src="img/icons/icon-down-arrow.png" onclick="openCloseSubmenu('open')" id="menu-down-arrow">
                    <input type="image" src="img/icons/icon-up-arrow.png" onclick="openCloseSubmenu('close')" id="menu-up-arrow">
                </div>
                <div class="top-navbar-submenu" id="submenu">
                    <a href="index.php#productos">Productos destacados</a>
                    <a href="index.php#noticias">Noticias</a>
                    <a href="index.php#contacto">Contacto</a>
                </div>
                <a href="">Catálogo</a>
            </div>

            <div id="cesta" class="cesta">
                <?php
                echo '<h3>Productes totals: '.$laMevaCistella->getNumProductes().'</h3>';
                ?>
                <div class="scrollable-products">
                <?php
                foreach($laMevaCistella->productes as $producte){
                    echo '<article class="item-cesta">
                            <section class="separator"></section>
                            <div>
                                <a href="detalle-producto.php?id-prod='.$producte->id.'" class="product-img-container"><img src='.$producte->fotos[0].'></a>
                                <label>'.$producte->titol.'</label>
                            </div>
                            <a>'.$producte->quantitat.' x '.$producte->preu.'€</a>
                            <div>
                                <div class="changeQuantity">
                                    <form action="controladores/controlador_cesta.php" method="post">
                                        <input type="hidden" name="id" value="'.$producte->id.'">
                                        <input type="submit" value="&nbsp;&nbsp" name="decrease" class="btn-cesta-minus">
                                    </form>
                                    <form action="controladores/controlador_cesta.php" method="post">
                                        <input type="hidden" name="id" value="'.$producte->id.'">
                                        <input type="submit" value="&nbsp;&nbsp" name="increase" class="btn-cesta-plus">
                                    </form>
                                </div>
                                <form action="controladores/controlador_cesta.php" method="post" class="deleteProd-btn">
                                    <input type="hidden" name="id" value="'.$producte->id.'">
                                    <input type="submit" value="&nbsp;&nbsp" name="delete">
                                </form>
                            </div>
                        </article>';
                }
                if($laMevaCistella->getTotal() == 0){
                    echo '<div class="cesta-empty">
                            <h4 class="no-margin-bottom">Añade algún producto a la cesta</h4>
                            <a href="productos.php" class="hover-naranja">Visitar catálogo</a>
                        </div>';
                }
                ?>
                </div>
                <?php
                echo '<section class="separator"></section>';
                echo '<h3>Total: '.$laMevaCistella->getTotal().'€</h3>';
                if($laMevaCistella->getTotal() != 0){
                    echo '<form action="checkout.php" method="post">
                            <input type="submit" value="Checkout" class="btn-submit cesta-checkout">
                        </form>';
                }
                ?>
            </div>

            <section class="container-producto" id="productos">
                <h2> Catálogo de productos</h2>
                <div class="productos-lista">
                    <?php

                    foreach($elMeuCataleg->productes as $producte){
                        echo '<form action="detalle-producto.php" method="get">
                            <article class="producto-item">
                                <a href="detalle-producto.php?id-prod='.$producte->id.'" class="center-content"><img src='.$producte->fotos[0].'></a>
                                <label class="cortar-texto producto-item-titulo">'.$producte->titol.'</label>
                                <p class="texto-naranja text-bold-bigger">'.$producte->preu.'€</p>
                                <p class="cortar-texto producto-item-descripcion no-margin">'.$producte->descripcio.'</p>
                                <p><b>'.$producte->categoria.'</b></p>
                                <div class="producto-rating">
                                    <p class="texto-naranja text-bold-bigger">'.$producte->showRating().'</p>
                                    <input type="hidden" id="id-prod" name="id-prod" value="'.$producte->id.'">
                                    <input type="submit" value="Más información" class="info-inpt">
                                </div>
                            </article>
                        </form>';
                    }
                    ?>
                </div>
            </section>
        </main>
        <footer>
            <div class="footer-title">
                <a href=""><img src="img/logo_icon_naranja.svg" class="logo-mobile mobile" href="#quienes_somos"></a>
                <h3>Contacta con nosotros</h3>
            </div>
            <section>
                <div>
                    <ul class="footer-lista">
                        <li><a>Teléfono: 934322910</a></li>
                        <li><a>Correo electrónico: <a href="https://mail.google.com/mail/?view=cm&fs=1&to=juanelectronics@gmail.com" target="_blank">juanelectronics@gmail.com</a></a></li>
                        <li><a>Dirección: Carrer del Pare Garí, 19</a></li>
                    </ul>
                </div>
                <div>
                    <a href="index.php"><img src="img/logo_juan_naranja.svg" class="logo desktop" href=""></a>
                    <div class="center-content">
                        <a href="admin/listado_productos.php">Administrar productos</a>    
                    </div>
                </div>
                <div class="rrss-icons">
                    <ul>
                        <li><a href="https://es-la.facebook.com/juanelectronic/" target="_blank"><img src="img/icons/facebook-icon.svg"></a></li>
                        <li><a href="https://www.instagram.com/juan.electronics/" target="_blank"><img src="img/icons/instagram-icon.svg"></a></li>
                        <li><a href="https://twitter.com/jelectronica" target="_blank"><img src="img/icons/twitter-icon.svg"></a></li>
                        <li><a href="https://api.whatsapp.com/send?phone=34616687926" target="_blank"><img src="img/icons/whatsapp-icon.svg"></a></li>
                    </ul>
                </div>
            </section>
        </footer>
    </body>
    <script src="scripts/header.js"></script> 
    <script src="scripts/cesta_visibility.js"></script>
</html>