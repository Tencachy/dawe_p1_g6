<?php
session_start();

//comprueba si los parámetros son correctos y lo guarda en una sesión
if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['login']) && !empty($_POST['login']))
{
    $username = $_POST['user'];
    $password = $_POST['password'];
    
    //si es correcto lleva a la lista de productos
    //si es incorrecto devuelve al login
    if($username == 'admin' && $password == 'admin'){
        $_SESSION['admin'] = true;
        header('Location:../admin/listado_productos.php');
    }else{
        $_SESSION['admin'] = false;
        header('Location:'.$_SERVER['HTTP_REFERER']);
    }
}

//logout de admin y redirige a index
if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['logout']) && !empty($_POST['logout']))
{
    $_SESSION['admin'] = false;
    header('Location:../index.php');
}
?>