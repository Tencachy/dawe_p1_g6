<?php
session_start();
include("../php/objectesProductes.php");

if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['increase']) && !empty($_POST['increase']))
{
    $productId = $_POST['id'];
    
    $laMevaCistella = unserialize($_SESSION['cistella']);

    $producteAfegit = $laMevaCistella->getProducteById($productId);
    $laMevaCistella->addProducte($producteAfegit);
    $_SESSION['cistella'] = serialize($laMevaCistella);
}
if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['decrease']) && !empty($_POST['decrease']))
{
    $productId = $_POST['id'];
    
    $laMevaCistella = unserialize($_SESSION['cistella']);

    $laMevaCistella->deleteSingleProducte($productId);
    $_SESSION['cistella'] = serialize($laMevaCistella);
}
if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['delete']) && !empty($_POST['delete']))
{
    $productId = $_POST['id'];
    
    $laMevaCistella = unserialize($_SESSION['cistella']);

    $laMevaCistella->deleteProducte($productId);
    $_SESSION['cistella'] = serialize($laMevaCistella);
}

if(isset($_SESSION['cistella'])){
    $laMevaCistella = unserialize($_SESSION['cistella']);
}

header('Location:'.$_SERVER['HTTP_REFERER']);
?>