<?php
session_start();
include("php/objectesProductes.php");
if(isset($_SESSION['cistella'])){
    $laMevaCistella = unserialize($_SESSION['cistella']);
}
//0) Declarem el namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
//1) Importem la bibioteca
require '../PHPMailer/src/Exception.php';
require '../PHPMailer/src/PHPMailer.php';
require '../PHPMailer/src/SMTP.php';

$nombre = $_POST['nombre'];
$calle = $_POST['calle'];
$email = $_POST['email'];
$pagament = $_POST['radios'];
$apellido = $_POST['apellido'];
$puerta = $_POST['puerta'];
$piso = $_POST['piso'];
$pais = $_POST['pais'];
$postal = $_POST['postal'];
$provincia = $_POST['provincia'];

$mail = new PHPMailer(true);
try {
    $mail->SMTPDebug = 0;         
    $mail->isSMTP();                     
    $mail->Host       = 'smtp.gmail.com';       
    $mail->SMTPAuth   = true;                            
    $mail->Username   = 'electronics.juan@gmail.com';     
    $mail->Password   = 'juanelectronicspassword';                   
    $mail->SMTPSecure = 'SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS';       
    $mail->Port       = 587;                              

    $mail->setFrom($mail->Username, 'Juan Electronics');
    $mail->addAddress($email);
    $mail->addReplyTo($mail->Username, 'Information');
    $mail->addCC($mail->Username);
    //$mail->addBCC('bcc@example.com');

    //$mail->addAttachment('/var/tmp/file.tar.gz');
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg'); 
    
    $mail->isHTML(true);
    $mail->Subject = 'Factura de compra - Juan Electronics';
    $variables = ['nombre'=>$nombre,
                'email'=>$email, 
                'pagament'=>$pagament, 
                'calle'=>$calle, 
                'apellido'=>$apellido, 
                'puerta'=>$puerta, 
                'piso'=>$piso, 
                'pais'=>$pais, 
                'postal'=>$postal, 
                'provincia'=>$provincia];
    $mail->Body = get_include_contents('../factura_cliente.php', $variables);
    $mail->AltBody = 'Aquest es el contingut del correu es pot formatejar am html pero no tots el lectors de correo ho permeten ';

    $mail->send();
    echo 'El missatge s \'ha enviat ';
} catch (Exception $e) {
    echo "Error al enviar el correu: {$mail->ErrorInfo}";
}

/*$adminmail = new PHPMailer();
try {
    echo 'INTENTANDO ENVIAR EL SEGUNDO MAIL';
    $adminmail->SMTPDebug = 1;
    $adminmail->isSMTP();
    $adminmail->Host       = 'smtp.gmail.com';
    $adminmail->SMTPAuth   = true;
    $adminmail->Username   = 'electronics.juan@gmail.com';
    $adminmail->Password   = 'juanelectronicspassword';
    $adminmail->SMTPSecure = 'SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS';
    $adminmail->Port       = 587;

    $adminmail->setFrom($adminmail->Username, 'Juan Electronics');
    $adminmail->addAddress('$adminmail->Username');
    $adminmail->addReplyTo($adminmail->Username, 'Information');

    $adminmail->isHTML(true);
    $adminmail->Subject = 'Segunda factura - Juan Electronics';
    $variables = ['nombre'=>$nombre,
                'email'=>$email, 
                'pagament'=>$pagament, 
                'calle'=>$calle, 
                'apellido'=>$apellido, 
                'puerta'=>$puerta, 
                'piso'=>$piso, 
                'pais'=>$pais, 
                'postal'=>$postal, 
                'provincia'=>$provincia];
    $adminmail->Body = get_include_contents('../factura_administrador.php', $variables);
    $adminmail->AltBody = 'Aquest es el contingut del correu es pot formatejar amb html pero no tots el lectors de correo ho permeten ';

    echo 'ANTES DE ENVIAR';
    $adminmail->send();
    echo 'El missatge s \'ha enviat admin';
} catch (Exception $e) {
    echo "Error al enviar el correu: {$adminmail->ErrorInfo}";
}*/

function get_include_contents($filename, $variablesToMakeLocal) {
    extract($variablesToMakeLocal);
    if (is_file($filename)) {
        ob_start();
        include $filename;
        return ob_get_clean();
    }
    return false;
}

header('Location:../compra_finalizada.php');
?>