function openCesta(){
  var cesta = document.getElementById('cesta');
  var menu = document.getElementById("top");
  if(cesta.style.display == 'none'){
    localStorage.setItem('visibilidad_cesta', 'block')
    cesta_visibilidad = localStorage.getItem('visibilidad_cesta')
    cesta.style.display = cesta_visibilidad;
    menu.style.display = 'none';
    console.log('Deberia estar block: '+cesta_visibilidad)
  }else{
    localStorage.setItem('visibilidad_cesta', 'none');
    cesta_visibilidad = localStorage.getItem('visibilidad_cesta')
    cesta.style.display = cesta_visibilidad;
    console.log('Deberia estar none: '+cesta_visibilidad)
  }
}

function setCestaVisibility(){
  var cesta = document.getElementById('cesta');
  cesta.style.display = 'none';
  if(localStorage.getItem('visibilidad_cesta') == 'block'){
    cesta.style.display = 'block';
  }else{
    cesta.style.display = 'none';
  }
}

setCestaVisibility();

function keepCestaOpened(){
  var cesta = document.getElementById('cesta');
  localStorage.setItem('visibilidad_cesta', 'block')
  cesta.style.display = 'block';
  console.log("Cesta open")
  return true;
}