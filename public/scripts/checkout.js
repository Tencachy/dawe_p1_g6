function setLength(id, tooltip) {
    var input = document.getElementById(id);
    var tooltip = document.getElementById(tooltip);
    if (input.value.length >= input.minLength) {
        input.setCustomValidity("");
        tooltip.style.display = "none";
    } else {
        input.setCustomValidity("Invalid field.");
        tooltip.style.display = "block";
    }
}
function validateCheckout() {

    if (invalidCount == 0) {
        return true;
    } else {
        return false;
    }
}