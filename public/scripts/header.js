function settopVisibility(){
  var menu = document.getElementById('top');
  menu.style.display = 'none';
  var submenu = document.getElementById('submenu');
  submenu.style.display = 'none';
  var menuImageDown = document.getElementById('menu-down-arrow');
  var menuImageUp = document.getElementById('menu-up-arrow');
  menuImageDown.style.display = 'block';
  menuImageUp.style.display = 'none';
}
settopVisibility();

function menu() {
  var menu = document.getElementById("top");
  var cesta = document.getElementById('cesta');
  if (menu.style.display === "none") {
    menu.style.display = "block";
    localStorage.setItem('visibilidad_cesta', 'none');
    cesta_visibilidad = localStorage.getItem('visibilidad_cesta')
    cesta.style.display = cesta_visibilidad;
  }else{
    menu.style.display = "none";
  }
}

window.addEventListener('resize', function(){
  if(this.window.innerWidth > 767){
    settopVisibility();
  }
}, true);

function openCloseSubmenu(open){
  var submenu = document.getElementById('submenu');
  var menuImageDown = document.getElementById('menu-down-arrow');
  var menuImageUp = document.getElementById('menu-up-arrow');
  if(open == 'open'){
    submenu.style.display = 'block';
    menuImageDown.style.display = 'none';
    menuImageUp.style.display = 'block';
  }else{
    submenu.style.display = 'none';
    menuImageDown.style.display = 'block';
    menuImageUp.style.display = 'none';
  }
}