<?php
include("model.php");

/*$producte1 = new Producte();
$producte1->titol = "POCO X3 Pro, Smartphone 8+256 GB, 6,67” 120 Hz FHD+ DotDisplay, Snapdragon 860, cámara cuádruple de 48 MP, 5160 mAh, Azul (versión ES/PT), incluye auriculares ";
$producte1->categoria = "Móviles";
$producte1->preu = 249.99;
$producte1->descripcio = "Performance: Qualcomm Snapdragon 860 | 7nm | Qualcomm Kryo 485, Octa-Core CPU, mit bis 2.96GHz Taktfrequenz | Qualcomm Adreno 640 GPU | LiquidCool Technology 1.0 Plus | LPDDR4X + UFS 3.1 Speicher | 6+128GB | MIUI 12 für POCO basierend auf Android 11 ";
$producte1->colors = ['negro', 'azul'];
$producte1->stoc = 19;
$producte1->caracteristiques = "TODO LO QUE TE ENCANTÓ DEL POCO X3, ¡Y MÁS! Conserva la pantalla suave como la seda, la batería enorme, la carga ultrarrápida y los altavoces con calidad de estudio. Hemos hecho las mejoras necesarias. ";
$producte1->especificacions = "Qualcomm Snapdragon 860 En comparación con el POCO X3 NFC, el POCO X3 Pro eleva el rendimiento al contar con el procesador Qualcomm Snapdragon 860 Mobile Platform, el procesador insignia 4G líder en 2021. Este potente procesador te permite disfrutar de los juegos móviles a gran velocidad, incluso cuando juegas a juegos exigentes en cuanto a configuraciones altas de gráficos.";
$producte1->instruccions = "Con la tecnología de memoria UFS 3.1 mejorada, el POCO X3 Pro ofrece velocidades de lectura y escritura muy rápidas. Carga juegos y aplicaciones sin tener que esperar. ";
$producte1->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_83404838/fee_786_587_png");
$producte1->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_83404843/fee_786_587_png");
$producte1->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_83404848/fee_786_587_png");
$producte1->valoracio = 5;


$producte2 = new Producte();
$producte2->titol = "Cargador rápido para iPhone 12 [Certificado MFi] Cargador de Pared de alimentación PD Tipo C de 20 W con Cable USB C de 4 pies a Lightning Compatible con iPhone13/12/12 Mini /12 Pro /12 Pro MAX /11 ";
$producte2->categoria = "Cargadores";
$producte2->preu = 14;
$producte2->descripcio = "[Cargador rápido PD 3.0 para iPhone] Cargador rápido para iPhone ANKUY equipado con enchufe de pared con suministro de energía PD 3.0 USB tipo C de 20W, puerto USB C de carga rápida PD 3.0 que proporciona una potencia de salida máxima de 20W, cargue su dispositivo hasta 3 veces más rápido que el cargador original de 5W ";
$producte2->colors = ['blanco'];
$producte2->stoc = 101  ;
$producte2->caracteristiques = "[Equipo Apple aplicable] Utilice el cable Lightning oficial del iPhone 12 con el cargador de pared 20W USB C para acceder a la carga rápida para iPhone";
$producte2->especificacions = "[Carga rápida y eficiente en cuanto a seguridad] El cargador de pared Power Delivery para iPhone posee un sistema de protección múltiple integrado, que evita sobretensión, sobrecarga, sobrecalentamiento, etc.";
$producte2->instruccions = "Cargue su dispositivo iPhone / iPad de forma rápida, segura y constante. La longitud de 6 pies sin enredos es conveniente para su uso mientras se carga y sincroniza en el hogar / oficina / viajes en la cama, habitaciones de hotel ";
$producte2->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_85918037/fee_786_587_png");
$producte2->addFoto("img/products/producto2-1.jpg");
$producte2->addFoto("img/products/producto2-3.jpg");
$producte2->valoracio = 4;

$producte3 = new Producte();
$producte3->titol = "SanDisk Extreme - Tarjeta de memoria microSDXC de 128 GB con adaptador SD, A2, hasta 160 MB/s, Class 10, U3 y V30 ";
$producte3->categoria = "Tarjetas de memoria";
$producte3->preu = 24.99;
$producte3->descripcio = "Hasta 160 MB/s de velocidad de lectura y 90 MB/s de velocidad de escritura para disparar y transferir rápido ";
$producte3->colors = ['negro'];
$producte3->stoc = 73;
$producte3->caracteristiques = "Categoría A2 para una carga y un rendimiento de la aplicación más rápidos ";
$producte3->especificacions = "Fabricadas para ser usadas en condiciones difíciles y sometidas a ensayos en dichas condiciones, resistentes a temperatura, agua, golpes y rayos X ";
$producte3->instruccions = "Ideal para smartphones y tabletas Android, cámaras de acción y drones ";
$producte3->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_85900077/fee_786_587_png");
$producte3->addFoto("img/products/producto3-2.jpg");
$producte3->addFoto("img/products/producto3-3.jpg");
$producte3->valoracio = 1;

$producte4 = new Producte();
$producte4->titol = "Altavoz inalámbrico - JBL Charge 5, 40 W, 20 horas, IP67, PartyBoost, USB Tipo-C";
$producte4->categoria = "Altavoces";
$producte4->preu = 149.99;
$producte4->descripcio = "Disfruta de la increíble potencia del sonido JBL Original Pro. El JBL Charge 5 incorpora un driver optimizado de gran amplitud, un altavoz de agudos independiente y dos radiadores de graves JBL que te proporcionan un sonido intenso y nítido. Consigue el sonido de las salas más grandes, incluso al aire libre. ";
$producte4->colors = ['negro', 'azul', 'turquesa', 'verde', 'gris'];
$producte4->stoc = 23;
$producte4->caracteristiques = "Es perfecto para ir a la piscina, ir al parque o incluso, a la montaña. El JBL Charge 5 es resistente al polvo y al agua gracias a la certificación IP67, para que puedas llevártelo a cualquier parte.";
$producte4->especificacions = "El JBL Charge 5 de JBL mantiene tu fiesta en marcha de día y de noche gracias a su increíble batería de 7500 mAh, que te ofrece una duración de hasta 20 horas de reproducción.";
$producte4->instruccions = "La función PartyBoost te permite conectar dos altavoces JBL compatibles para disfrutar de un sonido estéreo o más de dos para crear un entorno de sonido perfecto para una fiesta de verdad.";
$producte4->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_83542359/fee_786_587_png");
$producte4->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_82475517/fee_786_587_png");
$producte4->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_82475521/fee_786_587_png");
$producte4->destacat = true;
$producte4->valoracio = 5;


$producte5 = new Producte();
$producte5->titol = "Móvil - Samsung Galaxy A12 (2021), Negro, 32 GB, 3 GB RAM, 6.5 HD+, QuadCam, Exynos 850, 5000 mAh, Android 11";
$producte5->categoria = "Móviles";
$producte5->preu = 149.93;
$producte5->descripcio = "Si necesitas un móvil versátil y funcional al mismo tiempo que no quieres renunciar al rendimiento, no te pierdas la oportunidad de hacerte con tu móvil Samsung Galaxy A12 (2021) en color negro, de 3 GB de RAM y 32 GB de capacidad. Tecnología y componentes de calidad a tu alcance.";
$producte5->colors = ['negro','azul','blanco'];
$producte5->stoc = 10;
$producte5->caracteristiques = "El Galaxy A12 (2021) de Samsung, cuenta con cuatro cámaras que te permitirán hacer fotos mucho más claras, nítidas y de calidad profesional. Una cámara de 2 MP Macro, otra de 2 MP Profundidad, continuada con 5 MP Ultra Gran Angular y 48 MP Principal. La cámara Ultra Gran Angular dará más amplitud a tu mundo. Además, tiene una cámara frontal de 8 MP.";
$producte5->especificacions = "Si eres de los que vive cada día al máximo, la batería de larga duración del Galaxy A12 (2021) es tu aliada perfecta. Con 5.000 mAh, tu teléfono móvil tendrá energía para todo el día y podrás disfrutar sin límite de tus contenidos de entretenimiento. Juega, comparte y visualiza tus vídeos y streamings como nunca desde tu smartphone Samsung. Y si se acaba, devuélvele toda la energía con su carga rápida de 15W.";
$producte5->instruccions = "Sus 32 GB de almacenamiento interno, unido a que puedes ampliarlos mediante tarjeta microSD hasta 1 TB, harán que en tu Galaxy A12 (2021) no sobre nada, seguido a 3 GB de memoria RAM. Almacena todo lo que te importa sin temor a agotar el espacio.";
$producte5->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_86538793/fee_786_587_png");
$producte5->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_86538780/fee_786_587_png");
$producte5->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_86538786/fee_786_587_png");
$producte5->destacat = true;
$producte5->valoracio = 3;

$producte6 = new Producte();
$producte6->titol = "2 Unidades, Protector de Pantalla para iPhone 11 y iPhone XR ";
$producte6->categoria = "Protector de pantalla";
$producte6->preu = 5;
$producte6->descripcio = "Hecho 0,33 mm de espesor de alta calidad premium de vidrio templado bordes redondeados en exclusiva para iPhone 11";
$producte6->colors = ['transparente'];
$producte6->stoc = 90;
$producte6->caracteristiques = "Muy alta dureza: resistente a los arañazos de hasta 9H (más duro que un cuchillo); De alta respuesta, alta transparencia y alta transparencia ";
$producte6->especificacions = "Libre de polvo, huellas dactilares libre, instalación de una pulsación súper easiy, libre de burbujas ";
$producte6->instruccions = "Está recubierto en la parte posterior con un adhesivo de silicona sólido que facilita la instalación y fija la película firmemente para no afectar la sensibilidad ";
$producte6->addFoto("https://cdn.shopify.com/s/files/1/0066/9050/4822/products/ScreenProtector_11pro_27feedb6-3248-4d98-8c4d-d8a0cb8ee706.png?v=1602806235");
$producte6->addFoto("img/products/producto6-2.jpg");
$producte6->addFoto("img/products/producto6-3.jpg");
$producte6->valoracio = 3;

$producte7 = new Producte();
$producte7->titol = "Blukar Auriculares In Ear, Auriculares con Cable y Micrófono Headphone Sonido Estéreo para Galaxy, Huawei, XiaoMi, PC, MP3/MP4 Android y todos los dispositivos de auriculares de 3,5 mm ";
$producte7->categoria = "Auriculares";
$producte7->preu = 9.99;
$producte7->descripcio = "Sonido Dinámico Estéreo Claro：Altavoces integrados de alto rendimiento para un rango de frecuencia extendido, menor distorsión, alto rendimiento y almohadillas aislantes del ruido que eliminan el ruido ambiental. Te traigo un sonido cristalino y dinámico. ";
$producte7->colors = ['negro','blanco'];
$producte7->stoc = 99;
$producte7->caracteristiques = "Cómodo de Usar：ergonómico y cómodo en el diseño de la oreja que evita que se caiga. Dos almohadillas blandas para un ajuste personalizado, que ofrecen una colocación segura y una comodidad duradera.";
$producte7->especificacions = "Micrófono Incorporado y Control Remoto: El micrófono incorporado transmite voz de alta claridad para una conversación fluida. El diseño del botón en el cable le permite recibir llamadas con manos libres mientras escucha música a través del botón multifuncional sin acceso a su dispositivo. ";
$producte7->instruccions = "Construcción de Metal: Es más resistente a la corrosión, anti-envejecimiento, sólido y duradero; Su aleación especial puede transmitir sonido de alta definición con un mejor efecto.";
$producte7->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_79090314/fee_786_587_png");
$producte7->addFoto("img/products/producto7-2.jpg");
$producte7->addFoto("img/products/producto7-3.jpg");
$producte7->addFoto("img/products/producto7-4.jpg");
$producte7->valoracio = 4;

$producte8 = new Producte();
$producte8->titol = "Apple AirPods con estuche de carga (2.ª generación) ";
$producte8->categoria = "Auriculares";
$producte8->preu = 139.99;
$producte8->descripcio = "AirPods con estuche de carga: más de 24 horas de reproducción de audio y hasta 18 horas de conversación; AirPods (una única carga): hasta 5 horas de reproducción de audio y hasta 3 horas de conversación o 15 minutos en el estuche te dan hasta 3 horas de reproducción de audio";
$producte8->colors = ['blanco'];
$producte8->stoc = 199;
$producte8->caracteristiques = "Dos micrófonos con tecnología beamforming; Dos sensores ópticos; Acelerómetro con detección de movimiento; Acelerómetro con detección de voz";
$producte8->especificacions = "Micrófono Incorporado y Control Remoto: El micrófono incorporado transmite voz de alta claridad para una conversación fluida. El diseño del botón en el cable le permite recibir llamadas con manos libres mientras escucha música a través del botón multifuncional sin acceso a su dispositivo. ";
$producte8->instruccions = "Construcción de Metal: Es más resistente a la corrosión, anti-envejecimiento, sólido y duradero; Su aleación especial puede transmitir sonido de alta definición con un mejor efecto.";
$producte8->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_85726006/fee_786_587_png");
$producte8->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_76257240/fee_786_587_png");
$producte8->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_76257243/fee_786_587_png");
$producte8->destacat = true;
$producte8->valoracio = 4;

$producte9 = new Producte();
$producte9->titol = "Gritin Palo Selfie Trípode, 3 en 1 Selfie Stick Móvil Bluetooth con Inalámbrico Control Remoto";
$producte9->categoria = "Complementos";
$producte9->preu = 13;
$producte9->descripcio = "Palo Selfie Trípode 3 en 1: Usa brazos retráctiles de acero inoxidable de alta calidad, y el trípode y el selfie stick se combinan. ";
$producte9->colors = ['negro'];
$producte9->stoc = 199;
$producte9->caracteristiques = "Control Remoto Bluetooth: Selfie stick viene con un control remoto desmontable de la versión bluetooth 4.0, que se puede conectar fácil y rápidamente a bluetooth, logrando una conexión estable de las señales de bluetooth dentro de los 10 metros";
$producte9->especificacions = "Compacto y Ligero: El diseño de palo de selfie de una pieza puede estirarse hasta 70 cm cuando está completamente extendido y puede plegarse hasta 19.5 cm al mismo tiempo.";
$producte9->instruccions = "Ángulo Ajustable: Gritin palo de selfie bluetooth tiene un diseño de soporte giratorio de 270 °";
$producte9->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/pixelboxx-mss-80705540/fee_786_587_png");
$producte9->addFoto("img/products/producto9-1.jpg");
$producte9->addFoto("img/products/producto9-2.jpg");
$producte9->valoracio = 5;

$producte10 = new Producte();
$producte10->titol = "TV LED 24\" - LG 24TN510S-PZ, HD, Triple XD-Engine, Smart TV webOS 4.5, Virtual Surround, DVB-T2/C/S2";
$producte10->categoria = "TV";
$producte10->preu = 156.20;
$producte10->descripcio = "Disfruta de un Smart TV LG y Monitor LG a la vez. Los Monitores TV de LG tienen una doble finalidad: son la perfecta combinación entre televisor y monitor de ordenador. La resolución HD de los Monitores TV de LG te proporciona una calidad de imagen real con una magnífica precisión de color.";
$producte10->colors = ['negro'];
$producte10->stoc = 22;
$producte10->caracteristiques = "Disfruta de tus películas o juegos con un auténtico sonido estéreo Virtual Surround. Sistema operativo webOS 4.5: fácil, rápido y seguro para disfrutar de tus contenidos preferidos.";
$producte10->especificacions = "Calidad de color, contraste y mejora de la imagen gracias a los nuevos gráficos del Procesador Triple XD.";
$producte10->instruccions = "Tu solución sin cables. Conecta tus dispositivos a través de Miracast (Smartphone) y WiDi (PC) para compartir contenidos fácilmente.";
$producte10->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_83408099/fee_786_587_png");
$producte10->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_76626565/fee_786_587_png");
$producte10->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_76626570/fee_786_587_png");
$producte10->valoracio = 2;

$producte11 = new Producte();
$producte11->titol = "GoPro HERO10 Black - Cámara de acción a Prueba de Agua con LCD Frontal y Pantallas traseras táctiles, Video 5.3K60 Ultra HD, Fotos de 23MP, transmisión en Vivo de 1080p, cámara Web, estabilización ";
$producte11->categoria = "Cámaras";
$producte11->preu = 539;
$producte11->descripcio = "Procesador revolucionario: Más rápido. Más fluido. Mejor. El nuevo y potente motor GP2 supone toda una revolución: rendimiento rápido, controles táctiles adaptables y el doble de velocidad de fotogramas para capturar contenidos increíblemente fluidos.";
$producte11->colors = ['negro'];
$producte11->stoc = 30;
$producte11->caracteristiques = "Fotos y vídeos de alta resolución con alta velocidad de fotogramas: Da el salto a la GoPro más nítida hasta la fecha. Con unas increíbles fotos de 23 MP y vídeos con resolución 5,3K a 60 fps";
$producte11->especificacions = "Calidad de imagen increíble: Captura con detalles precisos, texturas realistas y un contraste sorprendente, incluso con poca luz. Las imágenes tienen un aspecto inmejorable gracias a la nueva cubierta de lente hidrófoba que repele el agua y ayuda a eliminar los brillos y otros artefactos. ";
$producte11->instruccions = "HyperSmooth 4.0: HyperSmooth nunca había sido tan fluido, y acceder a la mejor estabilización en todo momento resulta muy fácil. Además, podrás disfrutar de un rendimiento aún mejor con poca luz y alineación con el horizonte con un límite de inclinación superior, para que tus contenidos aparezcan perfectamente alineados cuando quieras. ";
$producte11->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_83425984/fee_786_587_png");
$producte11->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_81918196/fee_786_587_png");
$producte11->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_81918194/fee_786_587_png");
$producte11->valoracio = 4;

$producte12 = new Producte();
$producte12->titol = "Cámara réflex - Nikon D3500, Sensor CMOS, 24.2 MP, Full HD, Bluetooth + 18-55 mm f/3.5-5.6G";
$producte12->categoria = "Cámaras";
$producte12->preu = 412.40;
$producte12->descripcio = "Aventúrate en el mundo de la fotografía con la equipación precisa. Hazte ya con la cámara Réflex Nikon D3500 que contribuye a tu iniciación para que hagas tus mejores fotos. Captura todo lo que te rodea con esta cámara compacta y de gran rendimiento.";
$producte12->colors = ['verde'];
$producte12->stoc = 9;
$producte12->caracteristiques = "Nikon nos trae la D3500 con un diseño pequeño y de uso fácil para que puedas llevártela a todas partes. Con sólo una carga completa de su batería podrás capturar hasta 1550 disparos -ya que es de bajo consumo para que no se te escape ningún detalle. Además viene con Modo Guía el cual te indicará el ajuste que debes aplicar según el enfoque y el entorno. ";
$producte12->especificacions = "Dentro de las DSLR, hay una computadora especializada que mejora la precisión de los ajustes para cada disparo. El reconocido sistema de procesamiento de imagen EXPEED de Nikon se diseñó sobre la base de una rica experiencia en el tratamiento de imágenes y ayuda a la D3500 a tomar fotografías hermosas y vibrantes en casi cualquier condición de luz.";
$producte12->instruccions = "Grabar vídeos asombrosos con la D3500 es tan fácil como tomar fotos. Solo tiene que mover la palanca al modo de Vista en Vivo (Live View) y pulsar el botón de grabación. Capturará increíbles vídeos Full HD de 1080/60p de forma instantánea y sin esfuerzo. Use la capacidad de zoom de su lente para grabar videos en gran angular o primer plano.";
$producte12->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_80158362/fee_786_587_png");
$producte12->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/pixelboxx-mss-79982751/fee_786_587_png");
$producte12->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/pixelboxx-mss-79982754/fee_786_587_png");
$producte12->valoracio = 3;

$producte13 = new Producte();
$producte13->titol = "Wansview Cámara IP WiFi, 1080P Cámara Vigilancia WiFi con Visión Noturna Detección de Movimiento, Audio Bidireccional, Compatible con Alexa, Cámara de Seguridad Bebé Mascotas, Q6 (NO Tiene autonomía)  ";
$producte13->categoria = "Cámaras";
$producte13->preu = 31;
$producte13->descripcio = "Con lentes de gran ángulo de 110° y de 2 millones de pixeles, Wansview Q6 cámara de vigilancia presenta imágenes de mucha nitidez y 1080P videos. Gira 350° horizontalmente y 90° verticalmente";
$producte13->colors = ['blanco'];
$producte13->stoc = 55;
$producte13->caracteristiques = "Con micrófono y altavoz incorporados, nuestra cámara de vigilancia wifi interior soporta comunicaciones bidireccionales. Puede escuchar claramente y hablar desde su terminal con tus familiares.";
$producte13->especificacions = "Esta camara no puede grabar continuamente, solo hace grabaciones cuando detecta movimientos, y envía notificaciones en tiempo real a su móvil para advertir. Las grabaciones pueden guardarse de manera segura en la Tarjeta micro SD";
$producte13->instruccions = "Esta cámara de seguridad solo soporta conexión a 2.4G Wifi, 5G NO. Una vez conectada, puede controlarla remotamente en su móvil";
$producte13->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_82431699/fee_786_587_png");
$producte13->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_78938856/fee_786_587_png");
$producte13->addFoto("img/products/producto13-3.jpg");
$producte13->destacat = true;
$producte13->valoracio = 5;

$producte14 = new Producte();
$producte14->titol = "Tarjeta SDXC - SanDisk Ultra Plus, 64 GB, 130 MB/s, UHS-I, V10, Clase 10, Resistente al Agua, Multicolor";
$producte14->categoria = "Tarjetas de memoria";
$producte14->preu = 16;
$producte14->descripcio = "Amplia compatibilidad: Compatible con teléfonos inteligentes, tabletas, cámaras, cámaras de acción como GoPro, portátiles, ordenadores de sobremesa, cámaras réflex digitales, drones, Nintendo Switch y otras consolas portátiles";
$producte14->colors = ['negro'];
$producte14->stoc = 55;
$producte14->caracteristiques = "Almacenamiento de alta calidad: Perfecta para fotografías de alta resolución y grabar y almacenar vídeos Full HD/4K o de otro tipo ";
$producte14->especificacions = "Ultrarrápida: Velocidad de lectura de hasta 100 MB/s. ";
$producte14->instruccions = "Fiabilidad duradera: Resistencia IPX6 al agua, a los golpes y a temperaturas de -10° a 80°";
$producte14->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_85198458/fee_786_587_png");
$producte14->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_85198455/fee_786_587_png");
$producte14->addFoto("img/products/producto14-3.jpg");
$producte14->destacat = true;
$producte14->valoracio = 3;

$producte15 = new Producte();
$producte15->titol = "Walkie Talkie Profesional PMR USB Recargable, Walky Talky Adultos para Montaña Emisoras Caza, 8 Canales de Escaneo VOX Transmisores-receptores Radio";
$producte15->categoria = "Complementos";
$producte15->preu = 39.99;
$producte15->descripcio = "RADIOS DE 2 VÍAS: estos walkie-talkies tienen 8 canales, junto con el escaneo de canales para verificar la actividad. La placa frontal amarilla en negrita proporciona una gran visibilidad.";
$producte15->colors = ['negro'];
$producte15->stoc = 90;
$producte15->caracteristiques = "RANGO DE 5 MILLAS - Comunicación de mayor alcance en áreas abiertas con poca o ninguna obstrucción ";
$producte15->especificacions = "121 CÓDIGOS DE PRIVACIDAD DE CTCSS: el sistema de silenciamiento continuo codificado por tonos le ofrece hasta 2,662 opciones de canales para ayudar a bloquear otras conversaciones. Use la operación silenciosa para apagar todos los tonos ";
$producte15->instruccions = "Fiabilidad duradera: Resistencia IPX6 al agua, a los golpes";
$producte15->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/pixelboxx-mss-77332980/fee_786_587_png");
$producte15->addFoto("https://assets.mmsrg.com/isr/166325/c1/-/pixelboxx-mss-78680875/fee_786_587_png");
$producte15->addFoto("img/products/producto15-3.jpg");
$producte15->valoracio = 4;*/

$elMeuCataleg = new Cataleg();
/*$elMeuCataleg->addProducte($producte1);
$elMeuCataleg->addProducte($producte2);
$elMeuCataleg->addProducte($producte3);
$elMeuCataleg->addProducte($producte4);
$elMeuCataleg->addProducte($producte5);
$elMeuCataleg->addProducte($producte6);
$elMeuCataleg->addProducte($producte7);
$elMeuCataleg->addProducte($producte8);
$elMeuCataleg->addProducte($producte9);
$elMeuCataleg->addProducte($producte10);
$elMeuCataleg->addProducte($producte11);
$elMeuCataleg->addProducte($producte12);
$elMeuCataleg->addProducte($producte13);
$elMeuCataleg->addProducte($producte14);
$elMeuCataleg->addProducte($producte15);*/

$laMevaCistella = new Cistella();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//parámetros para la base de datos
$dbname = "javier-romero-7e3_botiga";
$user = "245101";
$password = "juanelectronics123";
$servidor = "mysql-javier-romero-7e3.alwaysdata.net";

//conecta a la base de datos
$conn = mysqli_connect($servidor, $user, $password, $dbname);
try {
    $dsn = "mysql:host=$servidor;dbname=$dbname";
    $laMevaConnexió = new PDO($dsn, $user, $password);
    $laMevaConnexió->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //per a que hem retorni tots el errors
} catch (PDOException $e) {
    echo $e->getMessage();
    echo "\nError fatal de la muerte mundial";
    exit();
}

//consuta para recoger los productos de la base de datos
$laMevaSentencia = $laMevaConnexió->prepare("SELECT * FROM productes");
$laMevaSentencia->setFetchMode(PDO::FETCH_CLASS , 'Producte');
$laMevaSentencia->execute();

//añade todos los productos de la base de datos al catálogo
while($elMeuProducte = $laMevaSentencia->fetch()){
   $elMeuProducte->fotos=[$elMeuProducte->foto1, $elMeuProducte->foto2, $elMeuProducte->foto3];
   $elMeuProducte->colors=[$elMeuProducte->colors];
   $elMeuCataleg->addProducte($elMeuProducte);
}
?>