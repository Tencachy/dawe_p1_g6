<?php

class Producte{
    public $id;
    public $titol;
    public $categoria;
    public $preu;
    public $descripcio;
    public $colors;
    public $stoc;
    public $caracteristiques;
    public $especificacions;
    public $instruccions;
    public $quantitat = 0;
    public $fotos;

    public $destacat = false;
    public $valoracio;

    /*static $currentId = 1;*/

    function Producte(){
        /*$this->id = self::$currentId;
        self::incrementId();*/
        $this->fotos = array();
    }

    function getId(){
        return $this->id;
    }

    function addFoto($url_foto){
        array_push($this->fotos, $url_foto);
    }

    /*static function incrementId(){
        self::$currentId++;
    }*/

    function showRating(){
        $estrellas = "";
        for($i = 0; $i<$this->valoracio; $i++){
            $estrellas = $estrellas . "&#9733;";
        }
        $resta = 5 - $this->valoracio;
        for($i = 0; $i<$resta; $i++){
            $estrellas = $estrellas . "&#9734;";
        }
        return $estrellas;
    }

    function arrayToString($array_colors){
        $string_colors = '';
        foreach($array_colors as $color){
            $string_colors = $string_colors . ' ' . $color;
        }
        return $string_colors;
    }

    function stringToArray($string_colors){
        return explode(" ", $string_colors);
    }

    function toArray(){
        $tmpArray = array(
            "titol"=>$this->titol,
            "categoria"=>$this->categoria,
            "preu"=>$this->preu,
            "descripcio"=>$this->descripcio,
            "colors"=>isset($this->colors[0]) ? $this->arrayToString($this->colors) : NULL,
            "stoc"=>$this->stoc,
            "caracteristiques"=>$this->caracteristiques,
            "especificacions"=>$this->especificacions,
            "instruccions"=>$this->instruccions,
            "foto1"=>isset($this->fotos[0]) ? $this->fotos[0] : NULL,
            "foto2"=>isset($this->fotos[1]) ? $this->fotos[1] : NULL,
            "foto3"=>isset($this->fotos[2]) ? $this->fotos[2] : NULL,
            "destacat"=>$this->destacat,
            "valoracio"=>$this->valoracio
        );
        return $tmpArray;
    }
}

class Cataleg{
    public $productes = [];

    function Cataleg(){
    }

    function addProducte($producte){
        $this->productes[] = $producte;
        /*echo "Producte afegit a catàleg: " . $producte->titol . "\n";*/
    }

    function getProducteById($id){
        foreach($this->productes as $producte){
            if($producte->id == $id){
                $producte_retornar = $producte;
            }
        }
        /*echo "Producte de id: ".$id."--".$producte_retornar->titol."\n\n";*/
        return $producte_retornar;
    }

    function llistar(){
        echo "Llista catàleg: \n";
        foreach($this->productes as $producte){
            echo "-ID: " . $producte->id . ".-" . $producte->titol . "\n";
        }
        echo "\n";
    }

    function getProductesDestacats(){
        $productes_destacats = [];
        foreach($this->productes as $producte){
            if($producte->destacat){
                array_push($productes_destacats, $producte);
            }
        }
        return $productes_destacats;
    }
}

class Cistella{
    public $productes = [];

    function Cistella(){
    }

    function addProducte($producte_nou){
        $existeix = false;
        foreach($this->productes as $producte){
            if($producte_nou->id == $producte->id){
                $existeix = true;
            }
        }
        if($existeix){
            $producte_nou->quantitat++;
        }else{
            $this->productes[] = $producte_nou;
            $producte_nou->quantitat++;   
        }
        /*echo "Producte afegit a cistella: " . $producte_nou->titol . " -- Quantitat: ". $producte_nou->quantitat."\n";*/
    }

    function deleteProducte($id){
        $producte = $this->getProducteById($id);
        $index = array_search($producte, $this->productes);
        array_splice($this->productes, $index, 1);
        $producte->quantitat = 0;
    }

    function deleteSingleProducte($id){
        foreach($this->productes as $producte){
            if($id == $producte->id){
                if($producte->quantitat>1){
                    $producte->quantitat--;
                }else{
                    $this->deleteProducte($id);
                }
            }
        }
    }

    function llistar(){
        echo "Llista cistella: \n";
        foreach($this->productes as $producte){
            echo "-ID: " . $producte->id . ".-" . $producte->titol . "\n";
        }
        echo "\n";
    }

    function getTotal(){
        $total_suma = 0;
        foreach($this->productes as $producte){
            $total_suma += $producte->preu*$producte->quantitat;
        }
        return $total_suma;
    }

    function getNumProductes(){
        $total_productes = 0;
        foreach($this->productes as $producte){
            $total_productes += $producte->quantitat;
        }
        //echo "Total productes: ".$total_productes."\n";
        return $total_productes;
    }

    function getProducteById($id){
        foreach($this->productes as $producte){
            if($producte->id == $id){
                $producte_a_retornar = $producte;
            }
        }
        /*echo "Producte amb id $id :".$producte_a_retornar->id."-- ".$producte_a_retornar->titol."\n";*/
        return $producte_a_retornar;
    }

    function buidar(){
        foreach($this->productes as $producte){
            $index = array_search($producte, $this->productes);
            array_splice($this->productes, $index, 1);
            $producte->quantitat=0;
        }
        /*echo "Cistella buida\n";*/
    }
}
?>