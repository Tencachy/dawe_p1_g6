<?php
session_start();
include("php/objectesProductes.php");
if(isset($_SESSION['cistella'])){
    $laMevaCistella = unserialize($_SESSION['cistella']);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Juan Electronics - Checkout</title>
    <link rel='stylesheet' href="css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel='stylesheet' href="css/guia.css">
    <link rel='stylesheet' href="css/menus.css">
    <link rel='stylesheet' href="css/checkout.css">
    <link rel='stylesheet' href="css/productos.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Maven+Pro:wght@400;700&family=Rubik:wght@400;700&display=swap" rel="stylesheet"> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
    <header>
        <img class="logo-navbar-mobile mobile" src="img/icons/icono-menu.png" onclick="menu()">
        <img class = "logo-juan" src="img/logo_juan.svg">
        <div class="top-navbar desktop">
            <div class="header-dropdown-menu">
                <a href="index.php">Inicio</a>
                <div class="header-dropdown-content">
                    <a href="index.php#productos">Productos destacados</a>
                    <a href="index.php#noticias">Noticias</a>
                    <a href="index.php#contacto">Contacto</a>
                </div>
            </div>                
            <a href="productos.php">Catálogo</a>
        </div>
    </header>
    <div id="cesta"></div>
    <main>
        <div id="top" class="top-navbar-mobile mobile">
            <div class="menu-index-point">
                <a href="index.php">Inicio</a>
                <input type="image" src="img/icons/icon-down-arrow.png" onclick="openCloseSubmenu('open')" id="menu-down-arrow">
                <input type="image" src="img/icons/icon-up-arrow.png" onclick="openCloseSubmenu('close')" id="menu-up-arrow">
            </div>
            <div class="top-navbar-submenu" id="submenu">
                <a href="index.php#productos">Productos destacados</a>
                <a href="index.php#noticias">Noticias</a>
                <a href="index.php#contacto">Contacto</a>
            </div>
            <a href="productos.php">Catálogo</a>
        </div>

    <!-- Form checkout -->
        <div class="col-12 m-0 row">
            <div class="row col-12 col-lg-8 col-xl-7 offset-xl-1 order-2 order-lg-1">
                <div class="col-12 mt-3 text-center">
                    <h2>Checkout</h2>
                </div>
                <div class="col-12">
                    <form class="m-3" action="controladores/controlador_checkout.php" method="post">
                        <div class="row">
                            <div class="form-group required col mb-3">
                                <label class="control-label h5 text-muted ">Nombre</label>
                                <input type="text" id="nombre" name="nombre" minlength="3" oninput="setLength('nombre', 'nombreTooltip')" class="form-control" required>
                                <small id="nombreTooltip" style="display: none">
                                    Minimo 3 caracteres
                                </small>
                            </div>
                            <div class="form-group required col mb-3">
                                <label class="control-label h5 text-muted">Apellido</label>
                                <input type="text" id="apellido" name="apellido" minlength="3" oninput="setLength('apellido','apellidoTooltip')" class="form-control" required>
                                <small id="apellidoTooltip" style="display: none">
                                    Minimo 3 caracteres
                                </small>
                            </div>
                        </div>
                        <div class="form-group required mb-3">
                            <label class="control-label h5 text-muted" for="exampleInputEmail1">Email</label>
                            <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="ejemplo@gmail.com" id="email" name="email" required>
                
                        </div>
                        <div class="form-group required  mb-3">
                            <label class="control-label h5 text-muted" for="exampleInputEmail1">Dirección</label>
                            <input type="text" id="direccion" name="calle" minlength="5" oninput="setLength('direccion','direccionTooltip')" class="form-control" aria-describedby="emailHelp" placeholder="C/ Ejemplo" required>
                            <small id="direccionTooltip" style="display: none">
                                Minimo 5 caracteres
                            </small>
                        </div>
                        <div class="row">
                            <div class="form-group required col mb-3">
                                <label class="control-label h5 text-muted">Puerta</label>
                                <input type="number" id="puerta" name="puerta" minlength="1" oninput="setLength('puerta','puertaTooltip')" class="form-control" placeholder="12" required>
                                <small id="puertaTooltip" style="display: none">
                                    No puede estar vacio
                                </small>
                            </div>
                            <div class="form-group required col mb-3">
                                <label class="control-label h5 text-muted">Piso</label>
                                <input type="text" id="piso" name="piso" minlength="1" oninput="setLength('piso','pisoTooltip')" class="form-control" placeholder="3º 4ª" required>
                                <small id="pisoTooltip" style="display: none">
                                    No puede estar vacio
                                </small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group required col-7 mb-3">
                            <label class="control-label h5 text-muted">Pais</label>
                            <select class="form-control" name="pais" required>
                                <option value="España">España</option>
                                <option value="Francia">Francia</option>
                                <option value="Portugal">Portugal</option>
                            </select>
                            </div>
                            <div class="form-group required col mb-3">
                                <label class="control-label h5 text-muted">Cod.Postal</label>
                                <input  type="number" id="codPostal" name="postal" minlength="3" oninput="setLength('codPostal','codPostalTooltip')" class="form-control" required>
                                <small id="codPostalTooltip" style="display: none">
                                    Minimo 3 caracteres
                                </small>
                            </div>
                        </div>

                        <div class="form-group required mb-3">
                        <label class="control-label h5 text-muted">Provincia</label>
                            <select class="form-control" name="provincia" required>
                                <option value="Girona">Girona</option>
                                <option value="Barcelona">Barcelona</option>
                                <option value="Tarragona">Tarragona</option>
                                <option value="Lleida">Lleida</option>
                            </select>
                        </div>

                        <hr>

                        <div class="col-12 mt-3 text-center">
                            <h2>Pago</h2>
                        </div>
                        <!-- Seleccion divisa -->
                        <div class="col-12 m-0 row">
                            <div class="form-check">
                                <input class="form-check-input btn-outline-orange" type="radio" name="radios" id="radios1" value="Bitcoin" checked>
                                <label class="form-check-label control-label h5 text-muted" for="radios1">
                                    Bitcoin
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input btn-outline-orange" type="radio" name="radios" id="radios2" value="Tarjeta">
                                <label class="form-check-label control-label h5 text-muted" for="radios2">
                                    Tarjeta
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input btn-outline-orange" type="radio" name="radios" id="radios3" value="Contra reembolso">
                                <label class="form-check-label control-label h5 text-muted" for="radios3">
                                    Contra reembolso
                                </label>
                            </div>
                        </div>

                        <hr>

                        <input type="submit" value="Finalizar Compra" class="offset-2 col-8 mt-2 offset-lg-4 col-lg-4 btn-submit">
                    </form>
                </div>
            </div>
            
            <div class="col-12 col-lg-4 col-xl-3 align-items-start order-1 order-lg-2">
                <div class="col-12 mt-3 text-center height-50">
                    <h2>Cesta</h2>
                </div>

                <div class="row col-10 offset-1 col-lg-12 offset-lg-0 mt-3">
                    <?php
                        foreach($laMevaCistella->productes as $producte){
                            echo '<div class="col-12 m-0 p-0 border row">
                                    <div class="col-4 align-self-center border-end">
                                        <img class="col-10 offset-1" src="'. $producte->fotos[0] .'">
                                    </div>
                                    
                                    <div class="col-8 align-self-center">
                                        <p class="col-12 h6 text-justify mt-1">' . $producte->titol .'</p>
                                        <p class="col-12 h6 text-muted text-end">'. $producte->quantitat .'x'. $producte->preu .'€</p>
                                    </div>
                                </div>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </main>
    <footer>
        <div class="footer-title">
            <a href=""><img src="img/logo_icon_naranja.svg" class="logo-mobile mobile" href=""></a>
            <h3>Contacta con nosotros</h3>
        </div>
        <section>
            <div>
                <ul class="footer-lista">
                    <li><a>Teléfono: 934322910</a></li>
                    <li><a>Correo electrónico: <a href="https://mail.google.com/mail/?view=cm&fs=1&to=juanelectronics@gmail.com" target="_blank">juanelectronics@gmail.com</a></a></li>
                    <li><a>Dirección: Carrer del Pare Garí, 19</a></li>
                </ul>
            </div>
            <a href=""><img src="img/logo_juan_naranja.svg" class="logo desktop" href=""></a>
            <div class="rrss-icons">
                <ul>
                    <li><a href="https://es-la.facebook.com/juanelectronic/" target="_blank"><img src="img/icons/facebook-icon.svg"></a></li>
                    <li><a href="https://www.instagram.com/juan.electronics/" target="_blank"><img src="img/icons/instagram-icon.svg"></a></li>
                    <li><a href="https://twitter.com/jelectronica" target="_blank"><img src="img/icons/twitter-icon.svg"></a></li>
                    <li><a href="https://api.whatsapp.com/send?phone=34616687926" target="_blank"><img src="img/icons/whatsapp-icon.svg"></a></li>
                </ul>
            </div>
        </section>
    </footer>
</body>
<script src="scripts/header.js"></script> 
<script src="scripts/checkout.js"></script> 
</html>