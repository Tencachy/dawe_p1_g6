<?php
$id = $_POST["id-delete"];

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//parámetros BBDD
$dbname = "javier-romero-7e3_botiga";
$user = "245101";
$password = "juanelectronics123";
$servidor = "mysql-javier-romero-7e3.alwaysdata.net";

//conexión en BBDD
$conn = mysqli_connect($servidor, $user, $password, $dbname);
try {
    $dsn = "mysql:host=$servidor;dbname=$dbname";
    $laMevaConnexió = new PDO($dsn, $user, $password);
    $laMevaConnexió->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo $e->getMessage();
    echo "\nError fatal de la muerte mundial";
    exit();
}

include('../../php/objectesProductes.php');

//borra las imágenes del producto eliminado de la carpeta de imágenes
$producte = $elMeuCataleg->getProducteById($id);
$foto1 = '../../' . $producte->fotos[0]; 
$foto2 = '../../' . $producte->fotos[1]; 
$foto3 = '../../' . $producte->fotos[2]; 

unlink($foto1);
unlink($foto2);
unlink($foto3);

//sentencia para eliminar producto
$laMevaSentencia = $laMevaConnexió->prepare(
    "DELETE FROM productes WHERE id = $id"
);
$laMevaSentencia->execute();
header('Location:'.$_SERVER['HTTP_REFERER']);
?>