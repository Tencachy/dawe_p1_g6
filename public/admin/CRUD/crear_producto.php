<?php
$titol = $_POST["titol"];
$categoria = $_POST["categoria"];
$preu = $_POST["preu"];
$descripcio = $_POST["descripcio"];
$colors = $_POST["colors"];
$stoc = $_POST["stoc"];
$caracteristiques = $_POST["caracteristiques"];
$especificacions = $_POST["especificacions"];
$instruccions = $_POST["instruccions"];
$destacat = $_POST["destacat"];
$valoracio = $_POST["valoracio"];

//selección de imagenes
$fotosDir = array_slice(scandir('../uploads/'), 2); 
$fotos = array_slice($fotosDir, 0, 3);

//moverlo a la carpeta uploads
$source = "../uploads/";
$destination = '../../img/products/';
foreach ($fotos as $file) {
  if (in_array($file, array(".",".."))) continue;
  if (copy($source.$file, $destination.$file)) {
    $delete[] = $source.$file;
  }
}

//borrar todas las imagenes de la carpeta uploads
$files = glob('../uploads/*'); 
foreach($files as $file) {
    if(is_file($file)) 
        unlink($file); 
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//parámetros BBDD
$dbname = "javier-romero-7e3_botiga";
$user = "245101";
$password = "juanelectronics123";
$servidor = "mysql-javier-romero-7e3.alwaysdata.net";

//conexión BBDD
$conn = mysqli_connect($servidor, $user, $password, $dbname);
try {
    $dsn = "mysql:host=$servidor;dbname=$dbname";
    $laMevaConnexió = new PDO($dsn, $user, $password);
    $laMevaConnexió->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //per a que hem retorni tots el errors
} catch (PDOException $e) {
    echo $e->getMessage();
    echo "\nError fatal de la muerte mundial";
    exit();
}

include('../../php/objectesProductes.php');

//crea nuevo producto con los datos del formulario
$producte = new Producte();
$producte->titol = $titol;
$producte->categoria = $categoria;
$producte->preu = $preu;
$producte->descripcio = $descripcio;
$producte->colors = $producte->stringToArray($colors);
$producte->stoc = $stoc;
$producte->caracteristiques = $caracteristiques;
$producte->especificacions = $especificacions;
$producte->instruccions = $instruccions;
//se añade la ruta de donde están guardadas las imágenes
$producte->addFoto('img/products/' . $fotos[0]);
$producte->addFoto('img/products/' . $fotos[1]);
$producte->addFoto('img/products/' . $fotos[2]);
if($destacat == 'true'){
    $producte->destacat = true;
}else{
    $producte->destacat = 0;
}
$producte->valoracio = $valoracio;

//sentencia para insertar producto
$laMevaSentencia = $laMevaConnexió->prepare(
    "INSERT INTO productes (titol, categoria, preu, descripcio, colors, stoc, caracteristiques, especificacions, instruccions, foto1, foto2, foto3, destacat, valoracio) 
    value ( :titol, :categoria, :preu, :descripcio, :colors, :stoc, :caracteristiques, :especificacions, :instruccions, :foto1, :foto2, :foto3, :destacat, :valoracio)"
);

$laMevaSentencia->execute($producte->toArray());
header('Location:../listado_productos.php');
?>