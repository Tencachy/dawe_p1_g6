<?php
$id = $_POST["id-update"];
$titol = $_POST["titol"];
$categoria = $_POST["categoria"];
$preu = $_POST["preu"];
$descripcio = $_POST["descripcio"];
$colors = $_POST["colors"];
$stoc = $_POST["stoc"];
$caracteristiques = $_POST["caracteristiques"];
$especificacions = $_POST["especificacions"];
$instruccions = $_POST["instruccions"];
$destacat = $_POST["destacat"];
$valoracio = $_POST["valoracio"];

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//parámetros BBDD
$dbname = "javier-romero-7e3_botiga";
$user = "245101";
$password = "juanelectronics123";
$servidor = "mysql-javier-romero-7e3.alwaysdata.net";

//conexión con BBDD
$conn = mysqli_connect($servidor, $user, $password, $dbname);
try {
    $dsn = "mysql:host=$servidor;dbname=$dbname";
    $laMevaConnexió = new PDO($dsn, $user, $password);
    $laMevaConnexió->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //per a que hem retorni tots el errors
} catch (PDOException $e) {
    echo $e->getMessage();
    echo "\nError fatal de la muerte mundial";
    exit();
}

include('../../php/objectesProductes.php');

//recogemos el producto antiguo para mantener las fotos
$oldproduct = $elMeuCataleg->getProducteById($id);

$producte = new Producte();
$producte->id = $id;
$producte->titol = $titol;
$producte->categoria = $categoria;
$producte->preu = $preu;
$producte->descripcio = $descripcio;
$producte->colors = $producte->stringToArray($colors);
$producte->stoc = $stoc;
$producte->caracteristiques = $caracteristiques;
$producte->especificacions = $especificacions;
$producte->instruccions = $instruccions;
//al editar el producto se mantienen las fotos antiguas
$producte->addFoto($oldproduct->fotos[0]);
$producte->addFoto($oldproduct->fotos[1]);
$producte->addFoto($oldproduct->fotos[2]);
if($destacat == 'true'){
    $producte->destacat = true;
}else{
    $producte->destacat = 0;
}
$producte->valoracio = $valoracio;

//sentencia para actualizar
$laMevaSentencia = $laMevaConnexió->prepare(
    "UPDATE productes SET titol = :titol, categoria = :categoria, preu = :preu, descripcio = :descripcio, colors = :colors, stoc = :stoc, 
    caracteristiques = :caracteristiques, especificacions = :especificacions, instruccions = :instruccions, foto1 = :foto1, foto2 = :foto2, foto3 = :foto3, 
    destacat = :destacat, valoracio = :valoracio WHERE id = ".$producte->id.";"
);

$laMevaSentencia->execute($producte->toArray());
header('Location:../listado_productos.php');
?>