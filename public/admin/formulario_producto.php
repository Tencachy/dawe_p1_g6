<?php
//si se intenta acceder directamente a esta página sin estar logeado redirige al login
session_start();
if($_SESSION['admin'] == false){
    header('Location:login.php');
}
include('../php/objectesProductes.php');
//se usa un boolean para saber si se crea un producto o se edita
$bool_edit = false;
if($_SERVER['REQUEST_METHOD'] == "POST" && !empty($_POST["edit"])){
    $bool_edit = true;
    $id = $_POST["id-edit"];
    $producte = $elMeuCataleg->getProducteById($id);
}

if($_SERVER['REQUEST_METHOD'] == "GET" && !empty($_GET["edit"])){
    $bool_edit = true;
    $id = $_GET["id-edit"];
    $producte = $elMeuCataleg->getProducteById($id);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel='stylesheet' href="../css/style.css">
    <link rel='stylesheet' href="../css/menus.css">
    <link rel='stylesheet' href="../css/productos.css">
    <link rel="stylesheet" href="../css/admin.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Maven+Pro:wght@400;700&family=Rubik:wght@400;700&display=swap" rel="stylesheet">
    <title>Juan Electronics - ADMIN</title>
</head>
<body style="background-color: #eeeeee;">
    <div class="container">
        <div class="row">
            <?php
            //crea la carpeta uploads
            if (!file_exists('uploads/')) {
                mkdir('uploads', 0777, true);
            }
            
            //mueve las imágenes cargadas a la carpeta uploads
            if(isset($_FILES['userfile'])){
                move_uploaded_file($_FILES['userfile']['tmp_name'], "uploads/" . $_FILES['userfile']['name']);
            }
            
            //borra la imagen de uploads
            if(isset($_GET['delete'])){
                unlink('uploads/' . $_GET['delete']);
            } 
            
            //muestra el formulario para añadir imágenes solo cuando se crea un producto, no al editar
            if(!$bool_edit){
                echo '<form id="upload-form" class="mt-3" enctype="multipart/form-data" method="POST">
                    <input id="file" name="userfile" type="file" onchange="document.getElementById(`upload-form`).submit()"/>
                    <label for="file">Sube las imágenes</label>
                </form>';
            }
            
            ?>
            <div class="file-grid">
            <?php
            if($bool_edit){
                foreach($producte->fotos as $foto){
                    $path = explode('/',$foto);
                    if($path[0] == 'img'){
                        echo "
                            <div class='file'>
                                <a href='../$foto'>
                                    <img src='../$foto'>
                                </a>
                            </div>
                        ";
                    }else{
                        echo "
                            <div class='file'>
                                <a href='$foto'>
                                    <img src='$foto'>
                                </a>
                            </div>
                        ";
                    }
                    
                }
            }else{
                if ($handle = opendir('uploads/')) {
                    while (($file = readdir($handle))) {
                        if ($file != "." && $file != "..") {
                            echo "
                                <div class='file'>
                                    <a href='uploads/$file'>
                                        <img src='uploads/$file'>
                                        <a class='trash' href='?delete=$file'></a>
                                        <p>$file</p>
                                    </a>
                                </div>
                        ";
                        }
                    }
                    closedir($handle);
                }
            } 
                
            ?>
            </div>
            <form <?php if($bool_edit){echo 'action="CRUD/actualizar_producto.php"';}else{echo 'action="CRUD/crear_producto.php"';} ?> 
            method="post" enctype="multipart/form-data" id="product-form">

            <?php if($bool_edit){echo '<input type="hidden" name="id-update" value="'. $producte->id .'">';} ?>
            <div class="row mt-5">
                <div class="form-group col-12 pl-0">
                    <label for="titol">Título</label>
                    <input type="text" class="form-control" name="titol" placeholder="Título"
                    <?php if($bool_edit){echo 'value="'.$producte->titol.'"';}else{echo 'value=""';} ?>>
                </div>
                <div class="row col-12 p-0 m-0">
                    <div class="form-group col-6 pl-0">
                        <label for="preu">Precio</label>
                        <input type="number" class="form-control" name="preu" placeholder="Precio" 
                        <?php if($bool_edit){echo 'value="'.$producte->preu.'"';}else{echo 'value=""';} ?>>
                    </div>
                    <div class="form-group col-6 pr-0">
                        <label for="stoc">Stock</label>
                        <input type="text" class="form-control" name="stoc" placeholder="Stock" 
                        <?php if($bool_edit){echo 'value="'.$producte->stoc.'"';}else{echo 'value=""';} ?>>
                    </div>
                </div>
                <div class="row col-12 m-0 p-0">
                    <div class="form-group col-9">
                        <label for="categoria">Categoría</label>
                        <select class="form-control" name="categoria" placeholder="Categoria">
                            <option <?php if($bool_edit){if($producte->categoria=="Móviles") echo 'selected';} ?>>Móviles</option>
                            <option <?php if($bool_edit){if($producte->categoria=="TV") echo 'selected';} ?>>TV</option>
                            <option <?php if($bool_edit){if($producte->categoria=="Vídeo") echo 'selected';} ?>>Vídeo</option>
                            <option <?php if($bool_edit){if($producte->categoria=="Audio") echo 'selected';} ?>>Audio</option>
                            <option <?php if($bool_edit){if($producte->categoria=="Almacenamiento") echo 'selected';} ?>>Almacenamiento</option>
                            <option <?php if($bool_edit){if($producte->categoria=="Complementos") echo 'selected';} ?>>Complementos</option>
                        </select>
                    </div>
                    <div style="margin-top: 2rem !important;" class="form-check col-2 offset-1">
                        <input class="form-check-input" type="checkbox" value="true" name="destacat" <?php 
                        if($bool_edit && $producte->destacat){echo 'checked';}?>>
                        <label class="form-check-label" for="destacat">
                            Destacado
                        </label>
                    </div>
                </div>
                <div class="row col-12 m-0 p-0">
                        <div class="form-group col-6">
                            <label for="valoracio">Valoración</label>
                            <input type="text" class="form-control" name="valoracio" placeholder="Valoración" <?php 
                            if($bool_edit){
                                echo 'value="'.$producte->valoracio.'"';
                            }else{
                                echo 'value=""';
                            } ?>>
                        </div>
                        <div class="form-group col-6">
                            <label for="colors">Colores</label>
                            <input type="text" class="form-control" name="colors" placeholder="Colores" <?php 
                            if($bool_edit){
                                echo 'value="'.$producte->arrayToString($producte->colors).'"';
                            }else{
                                echo 'value=""';
                            } ?>>
                        </div>
                    </div>
                <div class="form-group col-12">
                    <label for="descripcio">Descripción</label>
                    <textarea class="form-control" name="descripcio" placeholder="Descripción" rows="4"><?php 
                    if($bool_edit){
                        echo $producte->descripcio;
                    }?></textarea>
                </div>
                <div class="form-group col-12">
                    <label for="especificacions">Especificaciones</label>
                    <textarea class="form-control" name="especificacions" rows="4" placeholder="Especificaciones"><?php 
                    if($bool_edit){
                        echo $producte->especificacions;
                    }?></textarea>
                </div>
                <div class="form-group col-12">
                    <label for="caracteristiques">Características</label>
                    <textarea class="form-control" name="caracteristiques" rows="4" placeholder="Características"><?php 
                    if($bool_edit){
                        echo $producte->caracteristiques;
                    }?></textarea>
                </div>
                <div class="form-group col-12">
                    <label for="instruccions">Instrucciones</label>
                    <textarea class="form-control" name="instruccions" rows="4" placeholder="Instrucciones"><?php 
                    if($bool_edit){
                        echo $producte->instruccions;
                    }?></textarea>
                </div>
            </div>
            
            <div class="form-group col-12 mt-4 botones-editar">
                <input type="submit" name="submit" class="btn btn-success boton-guardar btn-lg" <?php if($bool_edit){echo 'value="Guardar"';}else{echo 'value="Añadir"';} ?>>
                <a href="listado_productos.php" class="btn btn-lg btn-danger ">Cancelar</a>
            </div>
            </form>
        </div>
    </div>
</body>
</html>