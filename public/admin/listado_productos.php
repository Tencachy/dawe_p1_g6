<?php
session_start();
if($_SESSION['admin'] == false){
    header('Location:login.php');
}
include("../php/objectesProductes.php");

//borrar todas las imagenes de la carpeta uploads
$files = glob('uploads/*'); 
foreach($files as $file) {
    if(is_file($file)) 
        unlink($file); 
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Juan Electronics - ADMIN</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel='stylesheet' href="../css/style.css">
    <link rel='stylesheet' href="../css/menus.css">
    <link rel='stylesheet' href="../css/productos.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Maven+Pro:wght@400;700&family=Rubik:wght@400;700&display=swap" rel="stylesheet">
</head>

<body style="background-color: #eeeeee;">
    <div class="container">
        <div class="row mt-4">
            <div class="col-3">
                <h2>Productos </h2>
            </div>
            <div class="col">
                <a class="btn btn-primary" href="formulario_producto.php">Añadir producto</a>
            </div>
            <div class="col-3">
                <form action="../controladores/controlador_login.php" method="POST">
                    <input type="submit" name="logout" class="btn btn-danger" value="Cerrar sesión de administrador">
                </form>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Stock</th>
                </tr>
            </thead>
            <tbody>
                <?php
                //muestra un listado con el título, precio, categoría y stock de cada producto
                foreach ($elMeuCataleg->productes as $producte) {
                    echo '<tr>
                            <th scope="row">' . $producte->titol . '</th>
                            <td>' . $producte->preu . '€</td>
                            <td>' . $producte->categoria . '</td>
                            <td>' . $producte->stoc . '</td>
                            <td>
                                <form action="formulario_producto.php" method="post">
                                    <input type="hidden" name="id-edit" value="'. $producte->id .'">
                                    <input class="editar" type="submit" value="&nbsp;&nbsp" name="edit">
                                </form>
                            </td>
                            <td>
                                <form action="CRUD/borrar_producto.php" method="post">
                                    <input type="hidden" name="id-delete" value="'. $producte->id .'">
                                    <input class="basura" type="submit" value="&nbsp;&nbsp" onclick="return confirm(`Estas seguro que deseas eliminar el producto?`)" name="delete">
                                </form>
                            </td>
                        </tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
</body>

</html>