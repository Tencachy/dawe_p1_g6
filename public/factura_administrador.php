<?php
session_start();
include("php/objectesProductes.php");
if(isset($_SESSION['cistella'])){
    $laMevaCistella = unserialize($_SESSION['cistella']);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' href="http://javier-romero-7e3.alwaysdata.net/css/style.css">
    <link rel='stylesheet' href="http://javier-romero-7e3.alwaysdata.net/css/guia.css">
    <link rel='stylesheet' href="http://javier-romero-7e3.alwaysdata.net/css/menus.css">
    <link rel='stylesheet' href="http://javier-romero-7e3.alwaysdata.net/css/productos.css">
    <link rel='stylesheet' href="http://javier-romero-7e3.alwaysdata.net/css/facturas.css">
    <title>Factura del Administrador</title>
    <style>
        * {
            border: 0;
            box-sizing: content-box;
            list-style: none;
            margin: 0;
            padding: 0;
            text-decoration: none;
            vertical-align: top;
        }

        /* content editable */

        * {
            border-radius: 0.25em;
            min-width: 1em;
            outline: 0;
        }

        * {
            cursor: pointer;
        }

        span {
            display: inline-block;
        }

        /* heading */

        h1 {
            font: bold 100% sans-serif;
            letter-spacing: 0.5em;
            text-align: center;
            text-transform: uppercase;
        }

        /* table */

        table {
            font-size: 75%;
            table-layout: fixed;
            width: 100%;
        }

        th,
        td {
            border-width: 1px;
            padding: 0.5em;
            position: relative;
            text-align: left;
        }

        th,
        td {
            border-radius: 0.25em;
        }

        th {
            background: #EEE;
            border-color: #BBB;
        }

        td {
            border-color: #DDD;
        }

        /* page */

        html {
            font: 16px 'Open Sans', sans-serif;
            overflow: auto;
            padding: 0.5in;
        }

        html {
            background: #999;
            cursor: default;
        }

        body {
            box-sizing: border-box;
            height: 11in;
            margin: 0 auto;
            overflow: hidden;
            padding: 0.5in;
        }

        body {
            background: #FFF;
            border-radius: 1px;
            box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
        }

        /* header */

        header {
            margin: 0 0 3em;
        }

        header:after {
            clear: both;
            content: "";
            display: table;
        }

        header h1 {
            background: #FF8519;
            border-radius: 0.25em;
            color: #FFF;
            margin: 0 0 1em;
            padding: 0.5em 0;
        }

        header address {
            float: left;
            font-size: 75%;
            font-style: normal;
            line-height: 1.25;
            margin: 0 1em 1em 0;
        }

        header address p {
            margin: 0 0 0.25em;
        }

        header span,
        header img {
            display: block;
            float: right;
        }

        header span {
            margin: 0 0 1em 1em;
            max-height: 25%;
            position: relative;
        }

        header img {
            max-height: 100%;
        }

        header input {
            cursor: pointer;
            height: 100%;
            left: 0;
            opacity: 0;
            position: absolute;
            top: 0;
        }

        /* article */

        article,
        article address,
        table.meta,
        table.inventory {
            margin: 0 0 3em;
        }

        article {
            float: left;
        }

        article h1 {
            clip: rect(0 0 0 0);
            position: absolute;
        }

        article address {
            float: left;
            font-size: 125%;
            font-weight: bold;
        }

        /* table meta & balance */

        table.meta,
        table.balance {
            float: right;
            width: 36%;
        }


        /* table meta */

        table.meta th {
            width: 40%;
        }

        table.meta td {
            width: 60%;
        }

        /* table items */

        table.inventory {
            clear: both;
            width: 100%;
        }

        table.inventory th {
            font-weight: bold;
            text-align: center;
        }

        /* table balance */

        table.balance th,
        table.balance td {
            width: 50%;
        }

        table.balance td {
            text-align: right;
        }

        @media print {
            * {
                -webkit-print-color-adjust: exact;
            }

            html {
                background: none;
                padding: 0;
            }

            body {
                box-shadow: none;
                margin: 0;
            }

            span:empty {
                display: none;
            }

        }

        @page {
            margin: 0;
        }
    </style>
</head>

<body>
<header>
        <h1>Factura Administrador</h1>
        <address style="width: 50%;"> 
            <p><?php echo $nombre . ' ' . $apellido ?></p>
            <p><?php echo $provincia . ', ' . $pais ?></p>
            <p><?php echo $calle . ', ' . $puerta . ', ' . $piso . '<br>' . $postal ?></p>
        </address>
        <span style="width: 25%;"><img style="width: 100%;" class ="logo-juan-factura" src="http://javier-romero-7e3.alwaysdata.net/img/logo_juan_naranja.png"></span>
    </header>
    <article>
        <address>
            <p>Juan Electronics</p>
        </address>
        <table class="meta">
            <tr>
                <th><span>Num.Factura</span></th>
                <td><span>1</span></td>
            </tr>
            <tr>
                <th><span>Fecha</span></th>
                <td><span><?php echo date('j F Y') ?></span></td>
            </tr>
            <tr>
                <th><span>Metodo de Pago</span></th>
                <td><span><?php echo $pagament ?></span></td>
            </tr>
        </table>
        <table class="inventory">
            <thead>
                <tr>
                    <th><span>Nombre</span></th>
                    <th><span>Precio</span></th>
                    <th><span>Cantidad</span></th>
                    <th><span>Total</span></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    foreach($laMevaCistella->productes as $producte){
                        echo '<tr>
                                <td style="border-bottom: 3px solid black"><span>'. $producte->titol .'</span></td>
                                <td style="border-bottom: 3px solid black"><span>'.$producte->preu.'</span><span>€</span></td>
                                <td style="border-bottom: 3px solid black"><span>'.$producte->quantitat.'</span></td>
                                <td style="border-bottom: 3px solid black"><span>'.$producte->quantitat * $producte->preu.'</span><span>€</span></td>
                            </tr>';
                    }
                ?>
            </tbody>
        </table>
        <table class="balance">
            <tr>
                <th><span>Total</span></th>
                <td><span><?php echo $laMevaCistella->getTotal() ?></span><span>€</span></td>
            </tr>
        </table>
    </article>              
    </main>
</body>

</html>