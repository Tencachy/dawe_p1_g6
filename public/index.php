<?php
session_start();
include("php/objectesProductes.php");
if(isset($_SESSION['cistella'])){
    $laMevaCistella = unserialize($_SESSION['cistella']);
}
?>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Juan Electronics</title>
        <link rel='stylesheet' href="css/style.css">
        <link rel='stylesheet' href="css/guia.css">
        <link rel='stylesheet' href="css/menus.css">
        <link rel='stylesheet' href="css/productos.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Maven+Pro:wght@400;700&family=Rubik:wght@400;700&display=swap" rel="stylesheet"> 
    </head>
    <body>
        <header>
            <img class="logo-navbar-mobile mobile" src="img/icons/icono-menu.png" onclick="menu()">
            <img class = "logo-juan" src="img/logo_juan.svg">
            <div class="top-navbar desktop">
                <!--<a href="#quienes_somos">Quiénes somos</a>-->
                <div class="header-dropdown-menu">
                <a href="">Inicio</a>
                    <div class="header-dropdown-content">
                        <a href="#productos">Productos destacados</a>
                        <a href="#noticias">Noticias</a>
                        <a href="#contacto">Contacto</a>
                    </div>
                </div>
                <a href="productos.php">Catálogo</a>
                <input  class="btn-carro" type="image" onclick="openCesta()" src="img/icons/cesta-icon.png"/>
            </div>   
            <input class="btn-carro-mobile mobile" type="image" onclick="openCesta()" src="img/icons/cesta-icon.png"/>
        </header>

        <main>
            <div id="top" class="top-navbar-mobile mobile">
                <div class="menu-index-point">
                    <a href="">Inicio</a>
                    <input type="image" src="img/icons/icon-down-arrow.png" onclick="openCloseSubmenu('open')" id="menu-down-arrow">
                    <input type="image" src="img/icons/icon-up-arrow.png" onclick="openCloseSubmenu('close')" id="menu-up-arrow">
                </div>
                <div class="top-navbar-submenu" id="submenu">
                    <a href="#productos">Productos destacados</a>
                    <a href="#noticias">Noticias</a>
                    <a href="#contacto">Contacto</a>
                </div>
                <a href="productos.php">Catálogo</a>
            </div>

            <div id="cesta" class="cesta">
                <?php
                echo '<h3>Productes totals: '.$laMevaCistella->getNumProductes().'</h3>';
                ?>
                <div class="scrollable-products">
                <?php
                foreach($laMevaCistella->productes as $producte){
                    echo '<article class="item-cesta">
                            <section class="separator"></section>
                            <div>
                                <a href="detalle-producto.php?id-prod='.$producte->id.'" class="product-img-container"><img src='.$producte->fotos[0].'></a>
                                <label>'.$producte->titol.'</label>
                            </div>
                            <a>'.$producte->quantitat.' x '.$producte->preu.'€</a>
                            <div>
                                <div class="changeQuantity">
                                    <form action="controladores/controlador_cesta.php" method="post">
                                        <input type="hidden" name="id" value="'.$producte->id.'">
                                        <input type="submit" value="&nbsp;&nbsp" name="decrease" class="btn-cesta-minus">
                                    </form>
                                    <form action="controladores/controlador_cesta.php" method="post">
                                        <input type="hidden" name="id" value="'.$producte->id.'">
                                        <input type="submit" value="&nbsp;&nbsp" name="increase" class="btn-cesta-plus">
                                    </form>
                                </div>
                                <form action="controladores/controlador_cesta.php" method="post" class="deleteProd-btn">
                                    <input type="hidden" name="id" value="'.$producte->id.'">
                                    <input type="submit" value="&nbsp;&nbsp" name="delete">
                                </form>
                            </div>
                          </article>';
                }
                if($laMevaCistella->getTotal() == 0){
                    echo '<div class="cesta-empty">
                            <h4 class="no-margin-bottom">Añade algún producto a la cesta</h4>
                            <a href="productos.php" class="hover-naranja">Visitar catálogo</a>
                        </div>';
                }
                ?>
                </div>
                <?php
                echo '<section class="separator"></section>';
                echo '<h3>Total: '.$laMevaCistella->getTotal().'€</h3>';
                if($laMevaCistella->getTotal() != 0){
                    echo '<form action="checkout.php" method="post">
                            <input type="submit" value="Checkout" class="btn-submit cesta-checkout">
                        </form>';
                }
                ?>
            </div>
            <section class="container-quienes-somos">
                <h2>Quiénes somos</h2>
                <div class="quienes-somos-descripcion">
                    <img src="img/juan-img.png">
                    <p>
                        Juan Electronics es una empresa familiar enfocada en la electrónica, situada en Trinitat Nova con más de 15 años de experiencia. 
                        El objetivo siempre ha sido el mismo, ofrecer los productos electrónicos de mejor calidad a un precio muy asequible. 
                        <br><br>
                        El negocio empezó siendo una tienda normal y corriente de barrio. Con el paso del tiempo hemos ido ofreciendo los productos más novedosos 
                        y eso nos ha posicionado como la mejor tienda de electrónica del barrio. Dándonos la oportunidad de expandirnos a otros barrios cercanos.
                    </p>
                </div>
            </section>

            <div class="nuestra-tienda">
                <h3>Nuestra tienda</h3>
            </div>

            <div class="mobile">
                  <img src="img/tienda-img3.png" class="img-slideshow tienda-mobile">
            </div>

            <div class="slideshow-container desktop">

                <div class="mySlides fade">
                  <img src="img/tienda-img1.png" class="img-slideshow">
                </div>
                
                <div class="mySlides fade">
                  <img src="img/tienda-img2.png" class="img-slideshow">
                </div>
                
                <div class="mySlides fade">
                  <img src="img/tienda-img3.png" class="img-slideshow">
                </div>
                
                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next" onclick="plusSlides(1)">&#10095;</a>
                
            </div>
            <br>
            <div class="desktop" style="text-align:center">
              <span class="dot" onclick="currentSlide(1)"></span> 
              <span class="dot" onclick="currentSlide(2)"></span> 
              <span class="dot" onclick="currentSlide(3)"></span> 
            </div>

            <section class="container-producto" id="productos">
                <h2>Productos destacados</h2>
                <div class="productos-lista">
                    <?php

                    foreach($elMeuCataleg->getProductesDestacats() as $producte){
                        echo '<form action="detalle-producto.php" method="get">
                            <article class="producto-item">
                                <a href="detalle-producto.php?id-prod='.$producte->id.'" class="center-content"><img src='.$producte->fotos[0].'></a>
                                <label class="cortar-texto producto-item-titulo">'.$producte->titol.'</label>
                                <p class="texto-naranja text-bold-bigger">'.$producte->preu.'€</p>
                                <p class="cortar-texto producto-item-descripcion no-margin">'.$producte->descripcio.'</p>
                                <p><b>'.$producte->categoria.'</b></p>
                                <div class="producto-rating">
                                    <p class="texto-naranja text-bold-bigger">'.$producte->showRating().'</p>
                                    <input type="hidden" id="id-prod" name="id-prod" value="'.$producte->id.'">
                                    <input type="submit" value="Más información" class="info-inpt">
                                </div>
                            </article>
                        </form>';
                    }
                    ?>
                </div>
            </section>

            <div class="container-noticias" id="noticias">
                <h2>Noticias de Interés</h2>
                <div class="noticias">
                    <div class="noticia-left">
                        <img src="img/noticia1.png">
                        <ul>
                            <li><h3>Un continuo subidón</h3></li>
                            <li class="desktop"><p>22/10/2021</p></li>
                            <div class="noticias-subtitulo">
                                <li><p>España</p></li>
                                <li><p>Raúl Rodríguez Vega</p></li>
                            </div>
                            <li><p>Sube el precio del gas, luego sube el precio de la factura de la luz; sube –o subirá– el precio de los alimentos, sube el precio del barril de petróleo, luego sube el precio de los carburantes (da igual gasolina o diésel); sube el precio de las materias primas y el transporte, luego subirá el precio de los juguetes. Solo una cosa baja, el peso de nuestros bolsillos.</p></li>
                            <li><a href="https://www.20minutos.es/opinion/raul-rodriguez-vega-continuo-subidon-20211022-4864214/" target="_blank" class="mas-info">Más información</a></li>
                        </ul>
                    </div>
                </div>
                <div class="noticias">
                    <div class="noticia-right">
                    <img class="mobile" src="img/noticia2.png">
                        <ul>
                            <li><h3>Un año sin luz en la Cañada Real</h3></li>
                            <li class="desktop"><p>22/10/2021</p></li>
                            <div class="noticias-subtitulo">
                                <li><p>España</p></li>
                                <li><p>Ignacio Domínguez</p></li>
                            </div>
                            <li><p>Se ha cumplido un año desde que la luz se apagó en el sector 6 de la Cañada Real Galiana de Madrid para más de 1.000 niños y niñas. <br>En estos 365 días han visto como los días se iban haciendo más cortos, han vivido la llegada del invierno y la tormenta Filomena, han visto brotar la primavera y han pasado las duras olas de calor del verano. Con la vuelta al cole, llegó el otoño, pero la luz no regresó.</p></li>
                            <li><a href="https://www.20minutos.es/opinion/ignacio-dominguez-mateos-un-ano-sin-luz-canada-real-20211022-4863852/" target="_blank">Más información</a></li>
                        </ul>
                        <img class="desktop" src="img/noticia2.png">
                    </div>
                </div>
            </div>
            </section>

            <section id="contacto">
                <h2>Dónde estamos</h2>
                <div class="container-mapa">
                    <iframe class="mapa"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3000.8207432938816!2d1.7168566156668963!3d41.22567671396683!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a386fe63b8b721%3A0x9ae1b896dcf1675!2sJoan%20Components%20Electronics!5e0!3m2!1sca!2ses!4v1634899060466!5m2!1sca!2ses"
                        allowfullscreen="" loading="lazy">
                    </iframe>
    
                    <ul class="info_mapa desktop">
                        <li type="disc"><a>Teléfono: 934322910</a></li>
                        <li type="disc"><a>Correo electrónico: <a href="https://mail.google.com/mail/?view=cm&fs=1&to=juanelectronics@gmail.com" target="_blank">juanelectronics@gmail.com</a></a></li>
                    </ul>
                </div>
            </section>
        </main>

        <footer>
            <div class="footer-title">
                <a href=""><img src="img/logo_icon_naranja.svg" class="logo-mobile mobile" href=""></a>
                <h3>Contacta con nosotros</h3>
            </div>
            <section>
                <div>
                    <ul class="footer-lista">
                        <li><a>Teléfono: 934322910</a></li>
                        <li><a>Correo electrónico: <a href="https://mail.google.com/mail/?view=cm&fs=1&to=juanelectronics@gmail.com" target="_blank">juanelectronics@gmail.com</a></a></li>
                        <li><a>Dirección: Carrer del Pare Garí, 19</a></li>
                    </ul>
                </div>
                <div>
                    <a href=""><img src="img/logo_juan_naranja.svg" class="logo desktop" href=""></a>
                    <div class="center-content">
                        <a href="admin/listado_productos.php">Administrar productos</a>    
                    </div>
                </div>
                <div class="rrss-icons">
                    <ul>
                        <li><a href="https://es-la.facebook.com/juanelectronic/" target="_blank"><img src="img/icons/facebook-icon.svg"></a></li>
                        <li><a href="https://www.instagram.com/juan.electronics/" target="_blank"><img src="img/icons/instagram-icon.svg"></a></li>
                        <li><a href="https://twitter.com/jelectronica" target="_blank"><img src="img/icons/twitter-icon.svg"></a></li>
                        <li><a href="https://api.whatsapp.com/send?phone=34616687926" target="_blank"><img src="img/icons/whatsapp-icon.svg"></a></li>
                    </ul>
                </div>
            </section>
        </footer>

    </body>
    <script src="scripts/header.js"></script> 
    <script src="scripts/slides.js"></script>
    <script src="scripts/cesta_visibility.js"></script>
</html>