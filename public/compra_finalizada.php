<?php
session_start();
include("php/objectesProductes.php");
if(isset($_SESSION['cistella'])){
    $laMevaCistella = unserialize($_SESSION['cistella']);
    $laMevaCistella->buidar();
    $_SESSION['cistella'] = serialize($laMevaCistella);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Juan Electronics - Checkout</title>
    <link rel='stylesheet' href="css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel='stylesheet' href="css/guia.css">
    <link rel='stylesheet' href="css/menus.css">
    <link rel='stylesheet' href="css/checkout.css">
    <link rel='stylesheet' href="css/productos.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Maven+Pro:wght@400;700&family=Rubik:wght@400;700&display=swap" rel="stylesheet"> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
    <header>
        <img class="logo-navbar-mobile mobile" src="img/icons/icono-menu.png" onclick="menu()">
        <img class = "logo-juan" src="img/logo_juan.svg">
        <div class="top-navbar desktop">
            <div class="header-dropdown-menu">
                <a href="index.php">Inicio</a>
                <div class="header-dropdown-content">
                    <a href="index.php#productos">Productos destacados</a>
                    <a href="index.php#noticias">Noticias</a>
                    <a href="index.php#contacto">Contacto</a>
                </div>
            </div>                
            <a href="productos.php">Catálogo</a>
        </div>
    </header>
    <div id="cesta"></div>
    <main>
        <div id="top" class="top-navbar-mobile mobile">
            <div class="menu-index-point">
                <a href="index.php">Inicio</a>
                <input type="image" src="img/icons/icon-down-arrow.png" onclick="openCloseSubmenu('open')" id="menu-down-arrow">
                <input type="image" src="img/icons/icon-up-arrow.png" onclick="openCloseSubmenu('close')" id="menu-up-arrow">
            </div>
            <div class="top-navbar-submenu" id="submenu">
                <a href="index.php#productos">Productos destacados</a>
                <a href="index.php#noticias">Noticias</a>
                <a href="index.php#contacto">Contacto</a>
            </div>
            <a href="productos.php">Catálogo</a>
        </div>

        <section>
            <div class="jumbotron ">
            <img class=" offset-2 mt-2  offset-lg-5 offset-md-4 px-5"  width="250" class="my-4" src="img/icons/delivery.gif"/>
                <h1 class="display-4 text-center">¡Gracias por tu Compra!</h1>
                <a class="btn btn-primary offset-2 col-8 mt-2 offset-lg-4 col-lg-4 btn-submit" href="index.php" role="button">Continuar</a>
            </div>
        </section>
    </main>
    <footer class="compra-finalizada-footer">
        <div class="footer-title">
            <a href=""><img src="img/logo_icon_naranja.svg" class="logo-mobile mobile" href=""></a>
            <h3>Contacta con nosotros</h3>
        </div>
        <section>
            <div>
                <ul class="footer-lista">
                    <li><a>Teléfono: 934322910</a></li>
                    <li><a>Correo electrónico: <a href="https://mail.google.com/mail/?view=cm&fs=1&to=juanelectronics@gmail.com" target="_blank">juanelectronics@gmail.com</a></a></li>
                    <li><a>Dirección: Carrer del Pare Garí, 19</a></li>
                </ul>
            </div>
            <a href=""><img src="img/logo_juan_naranja.svg" class="logo desktop" href=""></a>
            <div class="rrss-icons">
                <ul>
                    <li><a href="https://es-la.facebook.com/juanelectronic/" target="_blank"><img src="img/icons/facebook-icon.svg"></a></li>
                    <li><a href="https://www.instagram.com/juan.electronics/" target="_blank"><img src="img/icons/instagram-icon.svg"></a></li>
                    <li><a href="https://twitter.com/jelectronica" target="_blank"><img src="img/icons/twitter-icon.svg"></a></li>
                    <li><a href="https://api.whatsapp.com/send?phone=34616687926" target="_blank"><img src="img/icons/whatsapp-icon.svg"></a></li>
                </ul>
            </div>
        </section>
    </footer>
</body>
<script src="scripts/header.js"></script> 
<script src="scripts/checkout.js"></script> 
</html>